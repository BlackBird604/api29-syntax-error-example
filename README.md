# ExampleApp

- This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.0.2.
- It is the default Angular application with installed capacitor.
- It is used as a showcase for a runtime exception "SyntaxError: Unexpected token ?" occuring on an Android 10 device

## Reproduction

- Run `npm i` to install the dependencies
- Run `npm run android-prod` to build the project and open it in Android Studio
- Launch the app from Android Studio with an API 29 emulator
