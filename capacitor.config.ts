import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.example.app',
  appName: 'example-app',
  webDir: 'dist/example-app',
  bundledWebRuntime: false,
};

export default config;
